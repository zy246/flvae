import tensorflow as tf
from VAE_Client import FLClientModel
import config

tf.enable_eager_execution()

fl_model = FLClientModel()

fl_model.train(config.epochs)