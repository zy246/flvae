import numpy as np
import matplotlib.pyplot as plt
import pickle

#var_a = {'z':[1,2,43],'t_shape':(100,100),'m':4.998}
var_dict={}
var_dict['z'] = [1,2,3,4]
var_dict['t_shape'] = (100,100)
var_dict['m'] = 4.998
var_b = pickle.dumps(var_dict)
print('var_b:', var_b)

var_c = pickle.loads(var_b)
print('var_c', var_c['z'])
print(var_c['t_shape'])