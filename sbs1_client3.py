import tensorflow as tf
tf.enable_eager_execution()

from VAE_Client import TCPClient
import config
import numpy as np

s_index = 3 #5
s_data_size = config.data_size
dataset_name = config.dataset_name

if dataset_name == 'Netflix':
    client_index_list = [1957072, 1304711, 1199557, 1208469, 1408788, 2221610, 1106823, 1700761, 2045716, 1810310]
    training_data = np.loadtxt('data/Netflix_Data/training_3.txt')
    testing_data = np.loadtxt('data/Netflix_Data/testing_3.txt')

    tcp_client = TCPClient("127.0.0.1", 10369, s_index, client_index_list, s_data_size, dataset_name,training_data, testing_data)
    tcp_client.run(timeout=10000)

elif dataset_name == 'MovieLens':
    #client_index_list =[1,2]
    client_index_list =[1,2,3,4,5,6,7,8,9,10]
    training_data = []
    testing_data = []

    tcp_client = TCPClient("127.0.0.1", 10369,s_index,client_index_list,s_data_size,dataset_name,training_data,testing_data)
    tcp_client.run(timeout=10000)